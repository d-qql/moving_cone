#pragma once

#include <utils/utils.h>

inline mat4 lookAt(vec3 eye, vec3 at, vec3 up) {
    vec3 z = normalize(eye - at);
    vec3 x = normalize(cross(up, z));
    vec3 y = cross(z, x);
    return mat4
    (
        vec4(x.x, y.x, z.x, 0.f),
        vec4(x.y, y.y, z.y, 0.f),
        vec4(x.z, y.z, z.z, 0.f),
        vec4(-utils::transpose(mat3(x, y, z)) * eye, 1.f)
    );
}

inline mat4 projection(float near, float far, float ctgFOV, float aspectRatio) {
    float C1 = (far + near) / (far - near);
    float C2 = 2.f * far * near / (far - near);

    return mat4
    (
        vec4(ctgFOV / aspectRatio, 0.f, 0.f, 0.f),
        vec4(0.f, ctgFOV, 0.f, 0.f),
        vec4(0.f, 0.f, -C1, -1.f),
        vec4(0.f, 0.f, -C2, 0.f)
    );
}

inline vec3 cameraPosition(vec3 mouseXYZ) {
    float pi = 3.1415926535f;
    float phi = pi * mouseXYZ.x;
    float theta = 0.5f * pi * mouseXYZ.y;
    return mouseXYZ.z * vec3
           (
               std::cos(theta) * std::cos(phi),
               std::cos(theta) * std::sin(phi),
               std::sin(theta)
           );
}
