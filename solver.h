#pragma once
#include <array>
#include <complex>

#include "utils/utils.h"

inline std::array<std::complex<float>, 4> ellipseSurfaceEquation(mat4 surface, vec3 a, vec3 b){
    float e = std::sqrt(std::max(0.f, 1.f - dot(b, b) / dot(a, a)));
    mat3 A = mat3(surface);
    vec3 p = vec3(surface[3]);
    float c = surface[3].w;

    float K = dot(a, A * a);
    float R = dot(b, A * b);
    float Q = dot(a, A * b);
    float pb = dot(p, b);
    float pa = dot(p, a);

    std::complex<float> k4{(K - R) / 4.f, -Q / 2.f};
    std::complex<float> k3{pa - e * K, -pb + e * Q};
    std::complex<float> k2{(K + R) / 2.f + K * e * e - 2.f * e * pa + c, 0.f};
    std::complex<float> k1{pa - e * K, pb - e * Q};
    std::complex<float> k0{(K - R) / 4.f, Q / 2.f};


    return {k3 / k4, k2 / k4, k1 / k4, k0 / k4};
}

[[nodiscard]] inline std::array<std::complex<float>, 4> quarticSolver(
        std::complex<float> const& b, std::complex<float> const& c,
        std::complex<float> const& d, std::complex<float> const& e) noexcept {
        const std::complex<float> bd = b * d;
        const std::complex<float> bb = b * b;
        const std::complex<float> cc = c * c;
        const std::complex<float> Q1 =
            cc - static_cast<float>(3) * bd + static_cast<float>(12) * e;
        const std::complex<float> Q2 =
            static_cast<float>(2) * cc * c - static_cast<float>(9) * bd * c +
            static_cast<float>(27) * d * d + static_cast<float>(27) * bb * e -
            static_cast<float>(72) * c * e;
        const std::complex<float> Q3 =
            static_cast<float>(2) * b * (static_cast<float>(4) * c - bb) -
            static_cast<float>(16) * d;
        const std::complex<float> Q4 = static_cast<float>(3) * bb - static_cast<float>(8) * c;

        const std::complex<float> Q5 = std::pow(
            Q2 / static_cast<float>(2) + std::sqrt(Q2 * Q2 / static_cast<float>(4) - Q1 * Q1 * Q1),
            static_cast<float>(1) / static_cast<float>(3));

        const std::complex<float> Q6 =
            std::abs(Q5) > 0 ? (Q1 / Q5 + Q5) / static_cast<float>(3) : std::complex<float>(0, 0);
        const std::complex<float> Q7 =
            static_cast<float>(2) * std::sqrt(Q4 / static_cast<float>(12) + Q6);
        const std::complex<float> Q3Q7 = std::abs(Q7) > 0 ? Q3 / Q7 : std::complex<float>(0, 0);

        const std::complex<float> Q446    = static_cast<float>(4) * Q4 / static_cast<float>(6);
        const std::complex<float> Q64     = static_cast<float>(4) * Q6;
        const std::complex<float> bPlusQ7 = b + Q7;
        const std::complex<float> sqrt1   = std::sqrt(Q446 - Q64 - Q3Q7);

        const std::complex<float> x1 = (-bPlusQ7 - sqrt1) / static_cast<float>(4);
        const std::complex<float> x2 = (-bPlusQ7 + sqrt1) / static_cast<float>(4);

        const std::complex<float> Q7MinusB = Q7 - b;
        const std::complex<float> sqrt2    = std::sqrt(Q446 - Q64 + Q3Q7);
        const std::complex<float> x3       = (Q7MinusB - sqrt2) / static_cast<float>(4);
        const std::complex<float> x4       = (Q7MinusB + sqrt2) / static_cast<float>(4);

        return {x1, x2, x3, x4};
    }
