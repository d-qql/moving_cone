#pragma once
#include <array>
#include <vector>

#include "utils/utils.h"

inline vec2 conjugate(vec2 a) { return vec2(a.x, -a.y); }
inline float cnorm(vec2 a) { return a.x * a.x + a.y * a.y; }
inline float cabs(vec2 a) { return std::sqrt(cnorm(a)); }
inline vec2 crecp(vec2 a) { return conjugate(a) / cnorm(a); }
inline float carg(vec2 a) { return std::atan2(a.y, a.x); }

inline vec2 cFromAbsArg(float abs, float arg) { return abs * vec2(std::cos(arg), std::sin(arg)); }

inline vec2 cpow(vec2 a, float n) { return cFromAbsArg(std::pow(cabs(a), n), carg(a) * n); }

inline vec2 cmul(vec2 a, vec2 b) {
    return vec2
    (
        a.x * b.x - a.y * b.y,
        a.x * b.y + a.y * b.x
    );
}

inline vec2 cdiv(vec2 a, vec2 b) {
    return cmul(a, crecp(b));
}

inline vec2 csqrt(vec2 a) {
    // float r = cabs(a);
    // return vec2
    // (
    // std::sqrt(0.5f * std::max(0.f, a.x + r)),
    // (a.y > 0.f ? 1.f : a.y < 0.f ? -1.f : 0.f) * std::sqrt(0.5f * std::max(0.f, -a.x + r))
    // );
    return cpow(a, 0.5f);
}

inline std::array<vec2, 4> solverGlsl(vec2 b, vec2 c, vec2 d, vec2 e) {
    vec2 bd = cmul(b, d);
    vec2 bb = cmul(b, b);
    vec2 cc = cmul(c, c);
    vec2 Q1 = cc - 3.f * bd + 12.f * e;
    vec2 Q2 = 2.f * cmul(cc, c) - 9.f * cmul(bd, c) + 27.f * cmul(d, d) + 27.f * cmul(bb, e) - 72.f * cmul(c, e);
    vec2 Q3 = 2.f * cmul(b, 4.f * c - bb) - 16.f * d;
    vec2 Q4 = 3.f * bb - 8.f * c;

    vec2 Q5 = cpow(Q2 / 2.f + csqrt(cmul(Q2, Q2) / 4.f - cmul(Q1, cmul(Q1, Q1))), 1.f / 3.f);

    vec2 Q6 = cabs(Q5) > 0 ? (cdiv(Q1, Q5) + Q5) / 3.f : vec2(0.f);
    vec2 Q7 = 2.f * csqrt(Q4 / 12.f + Q6);
    vec2 Q3Q7 = cabs(Q7) > 0 ? cdiv(Q3, Q7) : vec2(0.f);

    vec2 Q446 = 4.f * Q4 / 6.f;
    vec2 Q64 = 4.f * Q6;
    vec2 bPlusQ7 = b + Q7;
    vec2 sqrt1 = csqrt(Q446 - Q64 - Q3Q7);

    vec2 x1 = (-bPlusQ7 - sqrt1) / 4.f;
    vec2 x2 = (-bPlusQ7 + sqrt1) / 4.f;

    vec2 Q7MinusB = Q7 - b;
    vec2 sqrt2 = csqrt(Q446 - Q64 + Q3Q7);

    vec2 x3 = (Q7MinusB - sqrt2) / 4.f;
    vec2 x4 = (Q7MinusB + sqrt2) / 4.f;

    return {x1, x2, x3, x4};
}

inline std::array<vec2, 4> ellipseSurfaceEquationGlsl(mat4 surface, vec3 a, vec3 b) {
    float e = std::sqrt(std::max(0.f, 1.f - dot(b, b) / dot(a, a)));
    mat3 A = mat3(surface);
    vec3 p = vec3(surface[3]);
    float c = surface[3].w;

    float K = dot(a, A * a);
    float R = dot(b, A * b);
    float Q = dot(a, A * b);
    float pb = dot(p, b);
    float pa = dot(p, a);

    vec2 k4 = vec2((K - R) / 4.f, -Q / 2.f);
    vec2 k3 = vec2(pa - e * K, -pb + e * Q);
    vec2 k2 = vec2((K + R) / 2.f + K * e * e - 2.f * e * pa + c, 0.f);
    vec2 k1 = vec2(pa - e * K, pb - e * Q);
    vec2 k0 = vec2((K - R) / 4.f, Q / 2.f);

    return {cdiv(k3, k4), cdiv(k2, k4), cdiv(k1, k4), cdiv(k0, k4)};
}

inline vec3 position(float E, vec3 semimajor, vec3 semiminor) {
    float a = utils::length(semimajor);
    float b = utils::length(semiminor);
    float ecc = std::sqrt(std::max(0.f, 1.f - b * b / (a * a)));
    return semimajor * (std::cos(E) - ecc) + semiminor * std::sin(E);
}

float pi = std::numbers::pi_v<float>;

inline bool containsPoint(ConeOriginDirection fig, vec3 point) {
    vec4 point4d = vec4(point, 1.f);
    return dot(point4d, fig.cone * point4d) < 0.f;
}

inline bool trueDirection(ConeOriginDirection fig, vec3 point) {
    return dot(point - fig.origin, fig.direction) > 0;
}

inline bool containsPointInTrueDirection(ConeOriginDirection fig, vec3 point) {
    return containsPoint(fig, point) && trueDirection(fig, point);
}

inline vec3 buildCenterPlaneNormal(vec3 coneOrigin) {
    return utils::normalize(utils::cross(vec3(0.f, 0.f, 1.f), utils::normalize(coneOrigin)));
}

inline bool possiblyContainsPoint(ConeOriginDirection uBig1, ConeOriginDirection uBig2, ConeOriginDirection startCone,
                                  ConeOriginDirection endCone, vec3 semimajor,
                                  vec3 semiminor,
                                  float uLat,
                                  float uAngle, float E) {
    vec3 pos = position(E, semimajor, semiminor);
    bool bigCones = false;
    bool timeCones = false;

    if (std::abs(uLat) <= uAngle) {
        bigCones = !containsPointInTrueDirection(uBig1, pos) && !containsPointInTrueDirection(uBig2, pos);
    }
    if (uLat > uAngle) {
        bigCones = containsPointInTrueDirection(uBig1, pos) && !containsPointInTrueDirection(uBig2, pos);
    }
    if (uLat < -uAngle) {
        bigCones = !containsPointInTrueDirection(uBig1, pos) && containsPointInTrueDirection(uBig2, pos);
    }

    if (std::abs(uLat) == pi / 2) {
        if(uAngle != pi / 2) return bigCones;
        if(uLat < 0) return !containsPointInTrueDirection(uBig1, pos) && containsPointInTrueDirection(uBig2, pos);
        if(uLat > 0) return containsPointInTrueDirection(uBig1, pos) && !containsPointInTrueDirection(uBig2, pos);
    }

    if (containsPointInTrueDirection(startCone, pos) || containsPointInTrueDirection(endCone, pos)) return bigCones;

    float posLon = std::fmod(float(atan2(pos.y, pos.x)) + 2 * pi, 2 * pi);
    float startLon = std::fmod(float(atan2(startCone.origin.y, startCone.origin.x)) + 2 * pi, 2 * pi);
    float endLon = std::fmod(float(atan2(endCone.origin.y, endCone.origin.x)) + 2 * pi, 2 * pi);
    if (startLon < endLon) {
        timeCones = startLon <= posLon && posLon <= endLon;
    } else {
        timeCones = startLon <= posLon || posLon <= endLon;
    }
    return bigCones && timeCones;
}

inline std::vector<bool> findTrueFragments(ConeOriginDirection uBig1, ConeOriginDirection uBig2,
                                           ConeOriginDirection startCone,
                                           ConeOriginDirection endCone,
                                           vec3 semimajor, vec3 semiminor,
                                           float uLat,
                                           float uAngle, std::vector<float> trueSortedEs) {
    std::vector<bool> fragments(trueSortedEs.size());
    for (uint i = 0; i < trueSortedEs.size(); ++i) {
        float centerE = i != trueSortedEs.size() - 1
                            ? (trueSortedEs[i % trueSortedEs.size()] + trueSortedEs[(i + 1) % trueSortedEs.size()]) / 2
                            : std::fmod(
                                (trueSortedEs[i % trueSortedEs.size()] + trueSortedEs[(i + 1) % trueSortedEs.size()]) /
                                2 + pi, 2.f * pi);
        fragments[i] = possiblyContainsPoint(uBig1, uBig2, startCone, endCone, semimajor, semiminor, uLat, uAngle,
                                             centerE);
    }
    return fragments;
}

std::vector<std::pair<float, float> > findIntervals(ConeOriginDirection uBig1, ConeOriginDirection uBig2,
                                                    ConeOriginDirection startCone,
                                                    ConeOriginDirection endCone,
                                                    vec3 semimajor, vec3 semiminor,
                                                    float uLat,
                                                    float uAngle, std::array<vec2, 4> roots1,
                                                    std::array<vec2, 4> roots2, std::array<vec2, 4> startRoots,
                                                    std::array<vec2, 4> endRoots) {
    std::vector<float> trueEs;
    for (uint i = 0; i < 4; i++) {
        float E1 = std::fmod(float(atan2(roots1[i].y, roots1[i].x)) + 2 * pi, 2 * pi);
        float E2 = std::fmod(float(atan2(roots2[i].y, roots2[i].x)) + 2 * pi, 2 * pi);
        float Estart = std::fmod(float(atan2(startRoots[i].y, startRoots[i].x)) + 2 * pi, 2 * pi);
        float Eend = std::fmod(float(atan2(endRoots[i].y, endRoots[i].x)) + 2 * pi, 2 * pi);
        // trueEs.push_back(E1);
        // trueEs.push_back(E2);

        if ( trueDirection(uBig1, position(E1, semimajor, semiminor))) {
            trueEs.push_back(E1);
        }
        if ( trueDirection(uBig2, position(E2, semimajor, semiminor))) {
            trueEs.push_back(E2);
        }
        trueEs.push_back(Estart);
        trueEs.push_back(Eend);
    }

    std::ranges::sort(trueEs);

    auto trueFragments = findTrueFragments(uBig1, uBig2, startCone, endCone, semimajor, semiminor, uLat, uAngle,
                                           trueEs);

    std::vector<uint> changes;
    for (uint i = 0; i < trueFragments.size(); ++i) {
        if (!(trueFragments[i] && trueFragments[(i + 1) % trueFragments.size()])) {
            changes.push_back(uint((i + 1) % trueFragments.size()));
        }
    }

    std::vector<std::pair<float, float> > intervals;
    if (changes.empty()) {
        if (possiblyContainsPoint(uBig1, uBig2, startCone, endCone, semimajor, semiminor, uLat, uAngle, 0.f)) {
            intervals.push_back({0, 2 * pi});
        }
        return intervals;
    }
    for (uint i = 0; i < changes.size(); ++i) {
        if (trueFragments[changes[i]]) {
            intervals.push_back({trueEs[changes[i]], trueEs[changes[(i + 1) % changes.size()]]});
        }
    }
    return intervals;
}

//
// inline bool checkTrueRoots(ConeOriginDirection cone, float lat, std::array<vec2, 4> roots) {
//     for(auto root: roots) {
//         if (std::abs((cabs(root) - 1.f)) < 1e-2f) {
//             if(std::abs(lat) != pi / 2) {
//                 buildCenterPlaneNormal(cone.origin);
//             }
//         }
//     }
// }
