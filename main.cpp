#include "gui.h"
#include "shader.h"
#include "window.h"
#include "cone_on_sphere.h"
#include "camera.h"
#include "solver.h"
#include "glslsolver.h"

#include <fstream>
#include <chrono>

int main() {
    Window window;
    GUI gui(window.pointer);

    ImGuiIO &io = ImGui::GetIO();
    io.FontGlobalScale = 1.f / std::min(window.xscale, window.yscale);
    io.Fonts->AddFontFromFileTTF(FONT_DIR "Roboto-Medium.ttf", 16.f / io.FontGlobalScale, nullptr,
                                 io.Fonts->GetGlyphRangesCyrillic());

    auto const readFile = [](char const *const filename) noexcept {
        std::ifstream in(filename);
        char buffer[16384] = {'\0'};
        in.read(buffer, sizeof(buffer));
        return std::string(buffer);
    };
    Shader const shader =
    {
        readFile(SHADER_DIR "fullscreen_quad.vert").c_str(),
        readFile(SHADER_DIR "fragment.glsl").c_str(),
    };

    Shader const orbitShader =
    {
        readFile(SHADER_DIR "orbit.vert").c_str(),
        readFile(SHADER_DIR "orbit_color.glsl").c_str()
    };

    unsigned int vao;
    glGenVertexArrays(1, &vao);

    auto const t0 = std::chrono::steady_clock::now();

    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);

    glBlendFunc(GL_DST_ALPHA, GL_ONE_MINUS_DST_ALPHA);

    glLineWidth(5.f);
    while (!window.shouldClose()) {
        int width, height;
        glfwGetFramebufferSize(window.pointer, &width, &height);

        glClearColor(0.f, 0.f, 0.f, 1.f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUseProgram(shader);

        vec3 origin = getOrigin(gui.earthRadius, gui.lat, gui.lon);
        vec3 direction = utils::normalize(origin);
        float angle = gui.angle;

        mat4 surface4d = buildCone(origin, direction, angle);

        vec3 startOrigin = getOrigin(gui.earthRadius, gui.lat, gui.startLon);
        vec3 startDirection = utils::normalize(startOrigin);

        vec3 endOrigin = getOrigin(gui.earthRadius, gui.lat, gui.endLon);
        vec3 endDirection = utils::normalize(endOrigin);

        ConeOriginDirection startCone{
            .cone = buildCone(startOrigin, startDirection, angle), .origin = startOrigin, .direction = startDirection
        };
        ConeOriginDirection endCone{
            .cone = buildCone(endOrigin, endDirection, angle), .origin = endOrigin, .direction = endDirection
        };


        auto [bigCone1, bigCone2] = globalCones(gui.earthRadius, gui.lat, gui.lon, gui.angle);

        gui.setBigCone1Matrix(bigCone1.cone);
        gui.setBigCone2Matrix(bigCone2.cone);
        gui.setDir1(bigCone1.direction);
        gui.setDir2(bigCone2.direction);
        // mat4 surface4d = surface4d_temp;

        float ctgFOV = 1.5f;
        float aspectRatio = float(width) / float(height);
        vec3 camPos = cameraPosition(vec3(gui.mouseX, gui.mouseY, gui.mouseZ));
        mat4 rotation = lookAt(camPos, vec3(0.f), vec3(0.f, 0.f, 1.f));
        mat4 proj = projection(0.5f, 100.f, ctgFOV, aspectRatio); {
            auto const t = std::chrono::steady_clock::now();
            std::chrono::duration<float, std::ratio<1> > const dt = t - t0;
            glUniform1f(glGetUniformLocation(shader, "uTime"), dt.count());
            glUniform1f(glGetUniformLocation(shader, "uCtgFOV"), ctgFOV);
            glUniform1f(glGetUniformLocation(shader, "uAspectRatio"), float(width) / float(height));
            glUniformMatrix4fv(glGetUniformLocation(shader, "uRotation"), 1, false, &rotation[0][0]);
            glUniform3f(glGetUniformLocation(shader, "uCamPos"), camPos.x, camPos.y, camPos.z);
            glUniform3f(glGetUniformLocation(shader, "uMouse"), gui.mouseX, gui.mouseY, gui.mouseZ);

            // glUniformMatrix4fv(glGetUniformLocation(shader, "uSurface"), 1, false, &surface4d[0][0]);
            glUniformMatrix4fv(glGetUniformLocation(shader, "uCone.surface"), 1, false, &surface4d[0][0]);
            glUniform3f(glGetUniformLocation(shader, "uCone.direction"), direction.x, direction.y, direction.z);
            glUniform3f(glGetUniformLocation(shader, "uCone.origin"), origin.x, origin.y, origin.z);

            glUniformMatrix4fv(glGetUniformLocation(shader, "uBig1.surface"), 1, false, &bigCone1.cone[0][0]);
            glUniform3f(glGetUniformLocation(shader, "uBig1.direction"), bigCone1.direction.x, bigCone1.direction.y,
                        bigCone1.direction.z);
            glUniform3f(glGetUniformLocation(shader, "uBig1.origin"), bigCone1.origin.x, bigCone1.origin.y,
                        bigCone1.origin.z);

            // glUniformMatrix4fv(glGetUniformLocation(shader, "uBigCone1"), 1, false, &bigCone1[0][0]);

            glUniformMatrix4fv(glGetUniformLocation(shader, "uBig2.surface"), 1, false, &bigCone2.cone[0][0]);
            glUniform3f(glGetUniformLocation(shader, "uBig2.direction"), bigCone2.direction.x, bigCone2.direction.y,
                        bigCone2.direction.z);
            glUniform3f(glGetUniformLocation(shader, "uBig2.origin"), bigCone2.origin.x, bigCone2.origin.y,
                        bigCone2.origin.z);

            glUniformMatrix4fv(glGetUniformLocation(shader, "uStartCone.surface"), 1, false, &startCone.cone[0][0]);
            glUniform3f(glGetUniformLocation(shader, "uStartCone.direction"), startCone.direction.x,
                        startCone.direction.y,
                        startCone.direction.z);
            glUniform3f(glGetUniformLocation(shader, "uStartCone.origin"), startCone.origin.x, startCone.origin.y,
                        startCone.origin.z);
            glUniformMatrix4fv(glGetUniformLocation(shader, "uEndCone.surface"), 1, false, &endCone.cone[0][0]);
            glUniform3f(glGetUniformLocation(shader, "uEndCone.direction"), endCone.direction.x, endCone.direction.y,
                        endCone.direction.z);
            glUniform3f(glGetUniformLocation(shader, "uEndCone.origin"), endCone.origin.x, endCone.origin.y,
                        endCone.origin.z);

            glUniform1f(glGetUniformLocation(shader, "uEarthRadius"), gui.earthRadius);
            glUniform1f(glGetUniformLocation(shader, "uEarthAlpha"), gui.earthAlpha);

            // glUniformMatrix4fv(glGetUniformLocation(shader, "uBigCone2"), 1, false, &bigCone2[0][0]);
        }

        glBindVertexArray(vao);
        glDrawArrays(GL_TRIANGLES, 0, 6);
        glBindVertexArray(0);

        ////////////////////////// ORBIT ///////////////////////////////
        glUseProgram(orbitShader);

        vec3 semimajor = vec3(7.f, 0.f, 0.f);
        vec3 semiminor = vec3(0.f, 0.f, 7.f);

        // auto eqCoefs = ellipseSurfaceEquation(bigCone2.cone, semimajor, semiminor);
        // auto solution = quarticSolver(eqCoefs[0], eqCoefs[1], eqCoefs[2], eqCoefs[3]);
        auto eqCoefsGlsl1 = ellipseSurfaceEquation(bigCone1.cone, semimajor, semiminor);
        auto solutionGlsl1 = quarticSolver(eqCoefsGlsl1[0], eqCoefsGlsl1[1], eqCoefsGlsl1[2], eqCoefsGlsl1[3]);
        auto eqCoefsGlsl2 = ellipseSurfaceEquation(bigCone2.cone, semimajor, semiminor);
        auto solutionGlsl2 = quarticSolver(eqCoefsGlsl2[0], eqCoefsGlsl2[1], eqCoefsGlsl2[2], eqCoefsGlsl2[3]);

        auto eqCoefsStart = ellipseSurfaceEquation(startCone.cone, semimajor, semiminor);
        auto eqCoefsEnd = ellipseSurfaceEquation(endCone.cone, semimajor, semiminor);
        auto solutionStart = quarticSolver(eqCoefsStart[0], eqCoefsStart[1], eqCoefsStart[2], eqCoefsStart[3]);
        auto solutionEnd = quarticSolver(eqCoefsEnd[0], eqCoefsEnd[1], eqCoefsEnd[2], eqCoefsEnd[3]);

        std::array<vec2, 4> s1 = {
            vec2(solutionGlsl1[0].real(), solutionGlsl1[0].imag()),
            vec2(solutionGlsl1[1].real(), solutionGlsl1[1].imag()),
            vec2(solutionGlsl1[2].real(), solutionGlsl1[2].imag()),
            vec2(solutionGlsl1[3].real(), solutionGlsl1[3].imag())
        };
        std::array<vec2, 4> s2 = {
            vec2(solutionGlsl2[0].real(), solutionGlsl2[0].imag()),
            vec2(solutionGlsl2[1].real(), solutionGlsl2[1].imag()),
            vec2(solutionGlsl2[2].real(), solutionGlsl2[2].imag()),
            vec2(solutionGlsl2[3].real(), solutionGlsl2[3].imag())
        };
        std::array<vec2, 4> s3 = {
            vec2(solutionStart[0].real(), solutionStart[0].imag()),
            vec2(solutionStart[1].real(), solutionStart[1].imag()),
            vec2(solutionStart[2].real(), solutionStart[2].imag()),
            vec2(solutionStart[3].real(), solutionStart[3].imag())
        };
        std::array<vec2, 4> s4 = {
            vec2(solutionEnd[0].real(), solutionEnd[0].imag()),
            vec2(solutionEnd[1].real(), solutionEnd[1].imag()),
            vec2(solutionEnd[2].real(), solutionEnd[2].imag()),
            vec2(solutionEnd[3].real(), solutionEnd[3].imag())
        };
        auto intervals = findIntervals(bigCone1, bigCone2, startCone, endCone, semimajor, semiminor, gui.lat, gui.angle,
                                       s1,
                                       s2, s3, s4);


        {
            auto const t = std::chrono::steady_clock::now();
            std::chrono::duration<float, std::ratio<1> > const dt = t - t0;
            glUniform1f(glGetUniformLocation(orbitShader, "uTime"), dt.count());
            glUniformMatrix4fv(glGetUniformLocation(orbitShader, "uRotation"), 1, false, &rotation[0][0]);
            glUniformMatrix4fv(glGetUniformLocation(orbitShader, "uProjection"), 1, false, &proj[0][0]);

            glUniformMatrix4fv(glGetUniformLocation(orbitShader, "uBig1.surface"), 1, false, &bigCone1.cone[0][0]);
            glUniform3f(glGetUniformLocation(orbitShader, "uBig1.direction"), bigCone1.direction.x,
                        bigCone1.direction.y,
                        bigCone1.direction.z);
            glUniform3f(glGetUniformLocation(orbitShader, "uBig1.origin"), bigCone1.origin.x, bigCone1.origin.y,
                        bigCone1.origin.z);
            glUniformMatrix4fv(glGetUniformLocation(orbitShader, "uBig2.surface"), 1, false, &bigCone2.cone[0][0]);
            glUniform3f(glGetUniformLocation(orbitShader, "uBig2.direction"), bigCone2.direction.x,
                        bigCone2.direction.y,
                        bigCone2.direction.z);
            glUniform3f(glGetUniformLocation(orbitShader, "uBig2.origin"), bigCone2.origin.x, bigCone2.origin.y,
                        bigCone2.origin.z);

            // glUniformMatrix4fv(glGetUniformLocation(orbitShader, "uSurface"), 1, false, &bigCone1.cone[0][0]);

            glUniform3f(glGetUniformLocation(orbitShader, "uSemimajor"), semimajor.x, semimajor.y, semimajor.z);
            glUniform3f(glGetUniformLocation(orbitShader, "uSemiminor"), semiminor.x, semiminor.y, semiminor.z);
            glUniform1i(glGetUniformLocation(orbitShader, "uOrbitPointsCount"), gui.pointsOnOrbit);

            glUniform1f(glGetUniformLocation(orbitShader, "uLat"), gui.lat);
            glUniform1f(glGetUniformLocation(orbitShader, "uAngle"), gui.angle);
            for (uint i = 0; i < intervals.size(); ++i) {
                glUniform1f(
                    glGetUniformLocation(orbitShader, ("uIC.intervals[" + std::to_string(i) + "].beginE").c_str()),
                    intervals[i].first);
                glUniform1f(
                    glGetUniformLocation(orbitShader, ("uIC.intervals[" + std::to_string(i) + "].endE").c_str()),
                    intervals[i].second);
            }
            glUniform1i(glGetUniformLocation(orbitShader, "uIC.count"), int(intervals.size()));
        }

        glBindVertexArray(vao);
        glDrawArrays(GL_LINE_LOOP, 0, gui.pointsOnOrbit);
        glBindVertexArray(0);


        gui.render();
    }
}
