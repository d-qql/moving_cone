#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <algorithm>
#include "utils/utils.h"

#include <imgui.h>
#include <imgui/backends/imgui_impl_glfw.h>
#include <imgui/backends/imgui_impl_opengl3.h>

struct GUI {
    GLFWwindow *window;

    float mouseX = 0.f, mouseY = 0.f, mouseZ = 5.f;
    bool show = true;

    float lat = 0.f;
    float lon = 0.f;
    float angle = std::numbers::pi_v<float> / 4.f;
    float earthRadius = 6.371f;
    float earthAlpha = 0.5f;

    mat4 bigCone1 = mat4(0.f);
    mat4 bigCone2 = mat4(0.f);
    vec3 dir1 = vec3(0.f);
    vec3 dir2 = vec3(0.f);
    int pointsOnOrbit = 200;

    float startLon = 0;
    float endLon = 0.4375274110194225f;

    GUI(GLFWwindow *const pWindow) noexcept;

    ~GUI() {
        ImGui_ImplOpenGL3_Shutdown();
        ImGui_ImplGlfw_Shutdown();
        ImGui::DestroyContext();
    }

    void render() noexcept;

    void setBigCone1Matrix(mat4 matrix) noexcept;

    void setBigCone2Matrix(mat4 matrix) noexcept;

    void setDir1(vec3 dir) noexcept;

    void setDir2(vec3 dir) noexcept;
};

inline void GUI::setDir1(vec3 dir) noexcept {
    dir1 = dir;
}

inline void GUI::setDir2(vec3 dir) noexcept {
    dir2 = dir;
}

inline void GUI::setBigCone1Matrix(mat4 matrix) noexcept {
    bigCone1 = matrix;
}

inline void GUI::setBigCone2Matrix(mat4 matrix) noexcept {
    bigCone2 = matrix;
}


inline GUI::GUI(GLFWwindow *const pWindow) noexcept
    : window(pWindow) {
    glfwSetWindowUserPointer(window, this);
    glfwSetKeyCallback(window, +[](GLFWwindow *const w, int key, int, int action, int) noexcept {
        if (action == GLFW_RELEASE)
            return;

        GUI &gui = *reinterpret_cast<GUI *>(glfwGetWindowUserPointer(w));
        if (key == GLFW_KEY_ESCAPE)
            gui.show = !gui.show;
    });

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 330 core");
}

inline void GUI::render() noexcept {
    //glfwSetInputMode(window, GLFW_CURSOR, show ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED);

    ImGuiIO &io = ImGui::GetIO();
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();

    if (!io.WantCaptureMouse) {
        float const dz = 1e-2f * io.MouseWheel;
        mouseZ = std::clamp(mouseZ * (1.f - dz), 0.1f, 100.f);

        if (io.MouseDown[0]) {
            auto const [dx, dy] = io.MouseDelta;

            float const m = 1e-3f;
            mouseX -= m * dx;
            mouseY = std::clamp(mouseY + m * dy, -0.999f, 0.999f);
        }
    }

    if (show) {
        ImGui::SetNextWindowPos(ImVec2(0.f, 0.f));
        ImGui::Begin("Информация", nullptr, ImGuiWindowFlags_NoMove);
        if (ImGui::Button("Quit"))
            glfwSetWindowShouldClose(window, GLFW_TRUE);
        ImGui::Text("%.2f ms, %.1f FPS", 1000.0f / io.Framerate, io.Framerate);
        ImGui::Text("ESC to toggle GUI focus");
        ImGui::Text("%g", io.MouseWheel);
        ImGui::SliderFloat("Альфа Земли", &earthAlpha, 0.f, 1.f);
        ImGui::SliderFloat("Радиус Земли", &earthRadius, 0.1f, 10.f);
        ImGui::SliderAngle("Долгота начала", &startLon, 0.f, 360.f);
        ImGui::SliderAngle("Долгота конца", &endLon, 0.f, 360.f);
        ImGui::SliderAngle("Широта", &lat, -90, 90);
        ImGui::SliderAngle("Долгота", &lon, 0, 360);
        ImGui::SliderAngle("Угол конуса", &angle, 0, 90);

        ImGui::SliderInt("Количество точек на орбите", &pointsOnOrbit, 20, 1000);
        ImGui::Text("Матрица большого конуса 1:\n"
                    "%.5f %.5f %.5f %.5f\n"
                    "%.5f %.5f %.5f %.5f\n"
                    "%.5f %.5f %.5f %.5f\n"
                    "%.5f %.5f %.5f %.5f\n",
                    bigCone1.x.x, bigCone1.y.x, bigCone1.z.x, bigCone1.w.x,
                    bigCone1.x.y, bigCone1.y.y, bigCone1.z.y, bigCone1.w.y,
                    bigCone1.x.z, bigCone1.y.z, bigCone1.z.z, bigCone1.w.z,
                    bigCone1.x.w, bigCone1.y.w, bigCone1.z.w, bigCone1.w.w
        );
        ImGui::Text("Направление большого конуса 1:\n"
                    "%.5f %.5f %.5f\n",
                    dir1.x, dir1.y, dir1.z
        );
        ImGui::Text("Матрица большого конуса 2:\n"
                    "%.5f %.5f %.5f %.5f\n"
                    "%.5f %.5f %.5f %.5f\n"
                    "%.5f %.5f %.5f %.5f\n"
                    "%.5f %.5f %.5f %.5f\n",
                    bigCone2.x.x, bigCone2.y.x, bigCone2.z.x, bigCone2.w.x,
                    bigCone2.x.y, bigCone2.y.y, bigCone2.z.y, bigCone2.w.y,
                    bigCone2.x.z, bigCone2.y.z, bigCone2.z.z, bigCone2.w.z,
                    bigCone2.x.w, bigCone2.y.w, bigCone2.z.w, bigCone2.w.w
        );
        ImGui::Text("Направление большого конуса 2:\n"
                    "%.5f %.5f %.5f\n",
                    dir2.x, dir2.y, dir2.z
        );
        ImGui::End();
    }

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}
