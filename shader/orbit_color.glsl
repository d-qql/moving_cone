#version 330 core

layout(location = 0) out vec4 outColor;

uniform vec3 uSemimajor;
uniform vec3 uSemiminor;

struct Interval{
    float beginE;
    float endE;
};

struct IntervalsAndCount{
    Interval[8] intervals;
    int count;
};

struct Cone{
    vec3 direction;
    vec3 origin;
    mat4 surface;
};

uniform Cone uBig1;
uniform Cone uBig2;
uniform float uLat;
uniform float uAngle;
uniform IntervalsAndCount uIC;

in float E;
in vec3 orbitPoint;

float pi = 3.1415926535f;

// vec2 -> complex float
vec2    conjugate   (vec2 a) { return vec2(a.x, -a.y); }
float   cnorm       (vec2 a) { return a.x * a.x + a.y * a.y; }
float   cabs        (vec2 a) { return sqrt(cnorm(a)); }
vec2    crecp       (vec2 a) { return conjugate(a) / cnorm(a); }
float   carg        (vec2 a) { return atan(a.y, a.x); }

vec2    cFromAbsArg  (float abs, float arg) { return abs * vec2(cos(arg), sin(arg)); }

vec2    cpow        (vec2 a, float n) { return cFromAbsArg(pow(cabs(a), n), carg(a) * n); }

vec2    cmul        (vec2 a, vec2 b)
{
    return vec2
    (
    a.x * b.x - a.y * b.y,
    a.x * b.y + a.y * b.x
    );
}
vec2    cdiv        (vec2 a, vec2 b)
{
    return cmul(a, crecp(b));
}
vec2    csqrt       (vec2 a)
{
    //    float r = cabs(a);
    //    return vec2
    //    (
    //    sqrt(0.5f * max(0.f, a.x + r)),
    //    sign(a.y) * sqrt(0.5f * max(0.f, -a.x + r))
    //    );
    return cpow(a, 0.5f);
}

vec2[4] solver(vec2 b, vec2 c, vec2 d, vec2 e){
    vec2 bd = cmul(b, d);
    vec2 bb = cmul(b, b);
    vec2 cc = cmul(c, c);
    vec2 Q1 = cc - 3.f * bd + 12.f * e;
    vec2 Q2 = 2.f * cmul(cc, c) - 9.f * cmul(bd, c) + 27.f * cmul(d, d) + 27.f * cmul(bb, e) - 72.f * cmul(c, e);
    vec2 Q3 = 2.f * cmul(b, 4.f * c - bb) - 16.f * d;
    vec2 Q4 = 3.f * bb - 8.f * c;
    vec2 Q5 = cpow(Q2 / 2.f + csqrt(cmul(Q2, Q2) / 4.f - cmul(Q1, cmul(Q1, Q1))), 1.f / 3.f);

    vec2 Q6 = cabs(Q5) > 0 ? (cdiv(Q1, Q5) + Q5) / 3.f : vec2(0.f);
    vec2 Q7 = 2.f * csqrt(Q4 / 12.f + Q6);
    vec2 Q3Q7 = cabs(Q7) > 0 ? cdiv(Q3, Q7) : vec2(0.f);

    vec2 Q446 = 4.f * Q4 / 6.f;
    vec2 Q64 = 4.f * Q6;
    vec2 bPlusQ7 = b + Q7;
    vec2 sqrt1 = csqrt(Q446 - Q64 - Q3Q7);

    vec2 x1 = (-bPlusQ7 - sqrt1) / 4.f;
    vec2 x2 = (-bPlusQ7 + sqrt1) / 4.f;

    vec2 Q7MinusB = Q7 - b;
    vec2 sqrt2 = csqrt(Q446 - Q64 + Q3Q7);

    vec2 x3 = (Q7MinusB - sqrt2) / 4.f;
    vec2 x4 = (Q7MinusB + sqrt2) / 4.f;

    return vec2[4](x1, x2, x3, x4);
}

vec2[4] ellipseSurfaceEquation(mat4 surface, vec3 a, vec3 b){
    float e = sqrt(1.f - dot(b, b) / dot(a, a));
    mat3 A = mat3(surface);
    vec3 p = vec3(surface[3]);
    float c = surface[3].w;

    float K = dot(a, A * a);
    float R = dot(b, A * b);
    float Q = dot(a, A * b);
    float pb = dot(p, b);
    float pa = dot(p, a);

    vec2 k4 = vec2((K - R) / 4.f, -Q / 2.f);
    vec2 k3 = vec2(pa - e * K, -pb + e * Q);
    vec2 k2 = vec2((K + R) / 2.f + K * e * e - 2.f * e * pa + c, 0.f);
    vec2 k1 = vec2(pa - e * K, pb - e * Q);
    vec2 k0 = vec2((K - R) / 4.f, Q / 2.f);

    return vec2[4](cdiv(k3, k4), cdiv(k2, k4), cdiv(k1, k4), cdiv(k0, k4));
}

float decodeSRGB(float x){
    float f = pow((x + 0.055f) / 1.055f, 2.4f);
    return f > 0.04045f ? f : x / 12.92f;
}

bool containsPoint(Cone fig, vec3 point){
    vec4 point4d = vec4(point, 1.f);
    return dot(point4d, fig.surface * point4d) < 0.f;
}

bool trueDirection(Cone fig, vec3 point){
    return dot(point - fig.origin, fig.direction) > 0;
}

bool containsPointInTrueDirection(Cone fig, vec3 point){
    return containsPoint(fig, point) && trueDirection(fig, point);
}

vec3 position(float E){
    float a = length(uSemimajor);
    float b = length(uSemiminor);
    float ecc = sqrt(1.f - b * b / (a * a));
    return uSemimajor * (cos(E) - ecc) + uSemiminor * sin(E);
}

float[8] sort8(float[8] numbers)
{
    float t;
    for (int i = 0; i < 7; ++i)
    {
        for (int j = i+1; j < 8; ++j)
        {
            if (numbers[j] < numbers[i])
            {
                t = numbers[i];
                numbers[i] = numbers[j];
                numbers[j] = t;
            }
        }
    }
    return numbers;
}

bool[8] findTrueFragments(float[8] sortedEs){
    bool[8] fragments;
    for (int i = 0; i < 8; ++i){
        float centerE = i != 7 ? (sortedEs[i % 8] + sortedEs[(i+1) % 8]) / 2 : mod((sortedEs[i % 8] + sortedEs[(i+1) % 8]) / 2 + pi, 2.f * pi);
        vec3 pos = position(centerE);
        if (abs(uLat) <= uAngle){
            if (!containsPointInTrueDirection(uBig1, pos) && !containsPointInTrueDirection(uBig2, pos)) fragments[i] = true;
            else fragments[i] = false;
        }
        else if (uLat > uAngle){
            if (containsPointInTrueDirection(uBig1, pos) && !containsPointInTrueDirection(uBig2, pos)) fragments[i] = true;
            else fragments[i] = false;
        }
        else if (uLat <= uAngle){
            if (!containsPointInTrueDirection(uBig1, pos) && containsPointInTrueDirection(uBig2, pos)) fragments[i] = true;
            else fragments[i] = false;
        }
    }
    return fragments;
}


IntervalsAndCount findIntervals(vec2[4] roots1, vec2[4] roots2){
    float[8] Es;
    for (int i = 0; i < 8; i+=2){
        Es[i] = mod(atan(roots1[i/2].y, roots1[i/2].x) + 2 * pi, 2 * pi);
        Es[i+1] = mod(atan(roots2[i/2].y, roots2[i/2].x) + 2 * pi, 2 * pi);
    }
    float[8] sortedEs = sort8(Es);
    bool[8] trueFragments = findTrueFragments(sortedEs);

    int[8] changes;
    int changesCount = 0;
    for (int i = 0; i < 8; ++i){
        if (!(trueFragments[i] && trueFragments[(i+1) % 8])){
            changes[changesCount] = (i + 1) % 8;
            ++changesCount;
        }
    }
    IntervalsAndCount ic;
    ic.count = 0;
    if (changesCount == 0) {
        if (trueFragments[0]){
            ic.count = 1;
            ic.intervals[0] = Interval(0, 0);
        } else {
            ic.count = 0;
        }
        return ic;
    }
    for (int i = 0; i < changesCount; ++i){
        if(trueFragments[changes[i]]){
            ic.intervals[ic.count] = Interval(Es[changes[i]], Es[changes[(i + 1) % changesCount]]);
            ++ic.count;
        }
    }
    return ic;
}

void main()
{

    outColor = vec4(0.1f, 0.1f, 0.1f, 1.f);
//        if (abs(uLat) <= uAngle){
//            if (!containsPointInTrueDirection(uBig1, orbitPoint) && !containsPointInTrueDirection(uBig2, orbitPoint)) outColor.g = 1.f;
//        }
//        else if (uLat > uAngle){
//            if (containsPointInTrueDirection(uBig1, orbitPoint) && !containsPointInTrueDirection(uBig2, orbitPoint)) outColor.g = 1.f;
//        }
//        else if (uLat <= uAngle){
//            if (!containsPointInTrueDirection(uBig1, orbitPoint) && containsPointInTrueDirection(uBig2, orbitPoint)) outColor.g = 1.f;
//        }

    vec2[4] ks1 = ellipseSurfaceEquation(uBig1.surface, uSemimajor, uSemiminor);
    vec2[4] xs1 = solver(ks1[0], ks1[1], ks1[2], ks1[3]);

    vec2[4] ks2 = ellipseSurfaceEquation(uBig2.surface, uSemimajor, uSemiminor);
    vec2[4] xs2 = solver(ks2[0], ks2[1], ks2[2], ks2[3]);

//    IntervalsAndCount ic = findIntervals(xs1, xs2);
    IntervalsAndCount ic = uIC;
    if (ic.count == 1 && ic.intervals[0].beginE == 0 && ic.intervals[0].endE == 0){
        outColor.b = 1.f;
    }

    for (int i = 0; i < ic.count; ++i){
        if (ic.intervals[i].beginE < ic.intervals[i].endE){
            if (ic.intervals[i].beginE <= mod(E + 2 * pi, 2 * pi) && mod(E + 2 * pi, 2 * pi) <= ic.intervals[i].endE){
                outColor.b = 1.f;
            }
        } else {
            if (ic.intervals[i].beginE <= mod(E + 2 * pi, 2 * pi) || mod(E + 2 * pi, 2 * pi) <= ic.intervals[i].endE){
                outColor.b = 1.f;
            }
        }
    }
    //
    //    for (int i = 0; i < 4; i++){
    //        if (
    //        abs(sin(E) - xs1[i].y) < 1e-2f
    //        && abs(cos(E) - xs1[i].x) < 1e-2f
    //        && abs((cabs(xs1[i]) - 1.f)) < 1e-2f
    //        )
    //        outColor = vec4(1.f, 0.f, 0.f, 0.5f);
    //
    //        if (
    //        abs(sin(E) - xs2[i].y) < 1e-2f
    //        && abs(cos(E) - xs2[i].x) < 1e-2f
    //        && abs((cabs(xs2[i]) - 1.f)) < 1e-2f
    //        )
    //        outColor = vec4(1.f, 1.f, 0.f, 0.5f);
    //    }
    //    outColor = vec4(cabs(xs[3]) / 2.f, 0.f, 0.f, 1.f);

}
