#version 330 core
in vec2 uv;
layout(location = 0) out vec4 outColor;

uniform float uTime;
uniform float uCtgFOV;
uniform float uAspectRatio;
uniform mat4 uRotation;
uniform vec3 uCamPos;
uniform vec3 uMouse;

uniform vec3 uConeDirection;

struct Cone{
    vec3 direction;
    vec3 origin;
    mat4 surface;
};

uniform Cone uCone;
uniform Cone uBig1;
uniform Cone uBig2;

uniform Cone uStartCone;
uniform Cone uEndCone;

uniform float uEarthRadius;
uniform float uEarthAlpha;

mat4 lookAt(vec3 eye, vec3 at, vec3 up)
{
    vec3 z = normalize(eye - at);
    vec3 x = normalize(cross(up, z));
    vec3 y = cross(z, x);
    return mat4
    (
    vec4(x.x, y.x, z.x, 0.f),
    vec4(x.y, y.y, z.y, 0.f),
    vec4(x.z, y.z, z.z, 0.f),
    vec4(-eye * mat3(x, y, z), 1.f)
    );
}

vec3 scaleFrom(float tgFOV, float aspectRatio)
{
    return vec3(tgFOV * aspectRatio, tgFOV, 1.f);
}

struct Ray
{
    vec3 origin;
    vec3 direction;
};

vec3 rayPoint(Ray ray, float t)
{
    return ray.origin + ray.direction * t;
}

Ray castRay(vec3 origin, mat3 cameraBasis, vec3 scale, vec2 uv)
{
    return Ray(
    origin,
    normalize(cameraBasis * (scale * vec3(uv, -1.f)))
    );
}

float axisX(Ray ray, float t)
{
    vec3 r = rayPoint(ray, t);
    return r.y * r.y + r.z * r.z - 1e-16f;
}

float dAxisXdT(Ray ray, float t)
{
    vec3 r = rayPoint(ray, t);
    return 2.f * dot(r, ray.direction) - 2.f * r.x * ray.direction.x;
}

float axisY(Ray ray, float t)
{
    vec3 r = rayPoint(ray, t);
    return r.x * r.x + r.z * r.z - 1e-16f;
}

float dAxisYdT(Ray ray, float t)
{
    vec3 r = rayPoint(ray, t);
    return 2.f * dot(r, ray.direction) - 2.f * r.y * ray.direction.y;
}

float axisZ(Ray ray, float t)
{
    vec3 r = rayPoint(ray, t);
    return r.x * r.x + r.y * r.y - 1e-16f;
}

float dAxisZdT(Ray ray, float t)
{
    vec3 r = rayPoint(ray, t);
    return 2.f * dot(r, ray.direction) - 2.f * r.z * ray.direction.z;
}

float intersectX(Ray ray)
{
    float t = 0;
    int iters = 0;

    while (abs(axisX(ray, t)) > 1e-5){
        if (iters > 10) return 0.f;
        t = t - axisX(ray, t) / dAxisXdT(ray, t);
        iters += 1;
    }
    return t;
}

float intersectY(Ray ray)
{
    float t = 0;
    int iters = 0;

    while (abs(axisY(ray, t)) > 1e-5){
        if (iters > 10) return 0.f;
        t = t - axisY(ray, t) / dAxisYdT(ray, t);
        iters += 1;
    }
    return t;
}

float intersectZ(Ray ray)
{
    float t = 0;
    int iters = 0;

    while (abs(axisZ(ray, t)) > 1e-5){
        if (iters > 10) return 0.f;
        t = t - axisZ(ray, t) / dAxisZdT(ray, t);
        iters += 1;
    }
    return t;
}


float surfaceValue(mat4 surface4d, Ray ray, float t)
{
    vec4 r = vec4(rayPoint(ray, t), 1.f);
    return dot(r, surface4d * r);
}

vec4 surface4dGrad(mat4 surface4d, vec4 r)
{
    return (transpose(surface4d) + surface4d) * r;
}

float surface4dGradDt(mat4 surface4d, Ray ray, float t){
    vec4 r = vec4(rayPoint(ray, t), 1.f);
    return dot(surface4dGrad(surface4d, r), vec4(ray.direction, 0.f));
}

float intersectSurface4D(mat4 surface, Ray ray)
{
    float t = 0;
    int iters = 0;

    while (abs(surfaceValue(surface, ray, t)) > 1e-5){
        if (iters > 50) return 0.f;
        t = t - surfaceValue(surface, ray, t) / surface4dGradDt(surface, ray, t);
        iters += 1;
    }
    vec3 r = rayPoint(ray, t);
    return t;
}

vec2 intersectSurface4dAll(mat4 surface, Ray ray){
    float t1 = 0;
    float t2 = 100;
    int iters = 0;

    while (abs(surfaceValue(surface, ray, t1)) > 1e-5){
        if (iters > 50){
            t1 = 0.f;
            break;
        }
        t1 = t1 - surfaceValue(surface, ray, t1) / surface4dGradDt(surface, ray, t1);
        iters += 1;
    }

    iters = 0;

    while (abs(surfaceValue(surface, ray, t2)) > 1e-5){
        if (iters > 50){
            t2 = 0.f;
            break;
        }
        t2 = t2 - surfaceValue(surface, ray, t2) / surface4dGradDt(surface, ray, t2);
        iters += 1;
    }

    return vec2(t1, t2);
}

float eraseHalf(Ray ray, float t, vec3 origin, vec3 direction){
    vec3 r = rayPoint(ray, t);
    return dot(r - origin, direction) < 0.f || length(r) > 0.5f * uMouse.z ? 0.f : t;
}

vec4 addColor(vec4 color, vec4 baseColor){
    return baseColor * baseColor.w + color * (1.f - baseColor.w);
}

vec4 formImage(vec4 sceneColor, Ray ray){
    mat4 earth = mat4(
    vec4(1.f, 0.f, 0.f, 0.f),
    vec4(0.f, 1.f, 0.f, 0.f),
    vec4(0.f, 0.f, 1.f, 0.f),
    vec4(0.f, 0.f, 0.f, -uEarthRadius * uEarthRadius)
    );
    vec4 earthColor = vec4(1.f, 1.f, 1.f, uEarthAlpha);
    const int conesCount = 5;
    Cone cones[conesCount] = Cone[](uCone, uBig1, uBig2, uStartCone, uEndCone);
    vec4 baseColors[conesCount] = vec4[](vec4(1.f, 1.f, 0.f, 0.7f), vec4(1.f, 0.f, 1.f, 0.7f), vec4(0.f, 1.f, 1.f, 0.7f), vec4(0.f, 1.f, 0.f, 0.7f), vec4(1.f, 0.f, 0.f, 0.7f));
    vec4 color = sceneColor;

    for (int i = 0; i < conesCount; i++){
        vec2 ts = intersectSurface4dAll(cones[i].surface, ray);
        ts = vec2(eraseHalf(ray, ts.x, cones[i].origin, cones[i].direction), eraseHalf(ray, ts.y, cones[i].origin, cones[i].direction));
        vec3 r1 = rayPoint(ray, ts.x);
        vec3 r2 = rayPoint(ray, ts.y);
        color = ts.x > 0.f ? addColor(color, vec4(fract(length(r1)) * vec3(baseColors[i]), baseColors[i].w)): color;
        color = ts.y > 0.f ? addColor(color, vec4(fract(length(r2)) * vec3(baseColors[i]), baseColors[i].w)): color;
    }
    vec2 earthTs = intersectSurface4dAll(earth, ray);
    vec3 r1 = rayPoint(ray, earthTs.x);
    vec3 r2 = rayPoint(ray, earthTs.y);
    color = earthTs.x > 0.f ? addColor(color, 0.5f * (fract(r1.x) + fract(r1.y)) * earthColor): color;
    color = earthTs.y > 0.f ? addColor(color, 0.5f * (fract(r2.x) + fract(r2.y)) * earthColor): color;
    return color;
}

void main()
{
    mat4 R = uRotation;
    float ctgFOV = uCtgFOV;


    Ray ray = castRay(uCamPos, transpose(mat3(uRotation)), scaleFrom(1./uCtgFOV, uAspectRatio), uv);

    outColor = intersectX(ray) != 0 ? vec4(1.f, 0.f, 0.f, 1.f) : vec4(vec3(0.f), 1.f);
    outColor = intersectY(ray) != 0 ? vec4(0.f, 1.f, 0.f, 1.f) : outColor;
    outColor = intersectZ(ray) != 0 ? vec4(0.f, 0.f, 1.f, 1.f) : outColor;

    //    float intersectT = intersectSurface4D(surface, ray);
    //    //    float normalLight = dot(conicNormal(ray, intersectT), normalize(vec3(0.f, 0.f, 1.f)));
    //    //    outColor = intersectT != 0 ? (normalLight > 0 ? vec4(vec3(normalLight), 1.f) : vec4(vec3(0.f), 1.f)) : outColor;
    //    vec4 intersectionPoint = vec4(rayPoint(ray, intersectT), 1.f);
    //    vec4 normal = normalize(surface4dGrad(surface, intersectionPoint));
    //    outColor = intersectT != 0 ? vec4(fract(intersectionPoint.x), fract(intersectionPoint.y), fract(intersectionPoint.z), 1.f) : outColor;
    //
    //    float bigConeT = intersectSurface4D(uBigCone1, ray);
    //    intersectionPoint = vec4(rayPoint(ray, bigConeT), 1.f);
    //    normal = normalize(surface4dGrad(uBigCone1, intersectionPoint));
    //    outColor = bigConeT != 0 ? vec4(normalize(vec3(intersectionPoint)), 1.f) : outColor;

    outColor = formImage(outColor, ray);
}
