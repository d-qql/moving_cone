#version 330 core

uniform float uTime;
uniform mat4 uRotation;
uniform mat4 uProjection;

uniform vec3 uSemimajor;
uniform vec3 uSemiminor;

uniform int uOrbitPointsCount;

out float E;
out vec3 orbitPoint;

vec3 position(float E){
    float a = length(uSemimajor);
    float b = length(uSemiminor);
    float ecc = sqrt(max(0.f, 1.f - b * b / (a * a)));
    return uSemimajor * (cos(E) - ecc) + uSemiminor * sin(E);
}

mat4 lookAt(vec3 eye, vec3 at, vec3 up)
{
    vec3 z = normalize(eye - at);
    vec3 x = normalize(cross(up, z));
    vec3 y = cross(z, x);
    return mat4
    (
    vec4(x.x, y.x, z.x, 0.f),
    vec4(x.y, y.y, z.y, 0.f),
    vec4(x.z, y.z, z.z, 0.f),
    vec4(-eye * mat3(x, y, z), 1.f)
    );
}

void main()
{
    float pi = 3.1415926535f;

    E = 2.f * pi / float(uOrbitPointsCount) * float(gl_VertexID);

    orbitPoint = position(E);

    gl_Position = uProjection * uRotation * vec4(orbitPoint, 1.f);
}
