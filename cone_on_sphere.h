#pragma once
#include "utils/utils.h"
#include <limits>

inline mat4 buildCone(vec3 origin, vec3 direction, float angle) {
    float cosa = std::cos(angle);
    float cosa2 = cosa * cosa;
    float od = utils::dot(origin, direction);

    mat3 A = utils::outerProduct(direction, direction) - cosa2 * utils::identity<float, 3>;
    vec3 b = origin * cosa2 - direction * od;
    float c = od * od - cosa2 * utils::dot(origin, origin);

    return -mat4(
        vec4(A.x, b.x),
        vec4(A.y, b.y),
        vec4(A.z, b.z),
        vec4(b, c)
    );
}

inline mat4 buildCylinder(vec3 origin, vec3 direction, float radius) {
    mat3 A =
            utils::identity<float, 3> - utils::outerProduct(direction, direction);
    vec3 b = -origin + direction * dot(direction, origin);
    float c = utils::dot(origin, origin) - dot(direction, origin) * dot(direction, origin) - radius * radius;
    return mat4(
        vec4(A.x, b.x),
        vec4(A.y, b.y),
        vec4(A.z, b.z),
        vec4(b, c)
    );
}

inline vec3 getOrigin(float radius, float lat, float lon) {
    return radius * vec3(std::cos(lat) * std::cos(lon), std::cos(lat) * std::sin(lon), std::sin(lat));
}

struct OriginDirectionAngle {
    vec3 origin;
    vec3 direction;
    float angle;
};

struct ConeOriginDirection {
    mat4 cone;
    vec3 origin;
    vec3 direction;
};

inline float sign(float num) {
    return std::signbit(num) ? -1.f : 1.f;
}


inline utils::gvec<ConeOriginDirection, 2> globalCones(float radius, float lat, float lon, float angle) {
    vec3 origin = getOrigin(radius, lat, lon);

    if (std::abs(lat) == std::numbers::pi_v<float> / 2.f) {
        return {
            ConeOriginDirection{
                .cone = buildCone(
                    vec3{0.f, 0.f, origin.z}, vec3{0.f, 0.f, 1.f}, angle
                ),
                .origin = vec3{0.f, 0.f, origin.z},
                .direction = vec3{0.f, 0.f, 1.f}
            },
            ConeOriginDirection{
                .cone = buildCone(
                    vec3{0.f, 0.f, origin.z}, vec3{0.f, 0.f, 1.f}, angle
                ),
                .origin = vec3{0.f, 0.f, origin.z},
                .direction = vec3{0.f, 0.f, -1.f}
            }
        };
    }

    vec3 direction = origin / radius; // всегда в надир

    vec3 up = vec3(0.f, 0.f, 1.f);
    vec3 planeNormal = utils::normalize(utils::cross(up, direction));
    vec3 right = utils::cross(up, planeNormal);

    mat3 toPlaneBasis = mat3(right, up, planeNormal);

    quat rq1 = quat(std::sin(angle / 2.f) * planeNormal, std::cos(angle / 2.f));
    quat rq2 = quat(std::sin(-angle / 2.f) * planeNormal, std::cos(-angle / 2.f));

    mat3 rotation1 = utils::fromQuaternion(rq1);
    mat3 rotation2 = utils::fromQuaternion(rq2);

    // Ближняя и дальняя образующие исходного конуса к оси 0z
    vec3 a1 = rotation1 * direction;
    vec3 a2 = rotation2 * direction;

    mat3 inverseToPlaneBasis = utils::transpose(toPlaneBasis);

    // Проекции ближних и дальних образующих и вершины конуса на базис плоскости.
    vec2 b1 = vec2(inverseToPlaneBasis * a1);
    vec2 b2 = vec2(inverseToPlaneBasis * a2);
    vec2 r0 = vec2(inverseToPlaneBasis * origin);

    // Пересечение проецированных образующих с осью 0z

    auto intersect = [](vec2 b, vec2 r)-> float {
        return (b.x * r.y - b.y * r.x) / b.x;
    };

    float bc1OriginZ = intersect(b1, r0);
    float bc2OriginZ = intersect(b2, r0);

    ConeOriginDirection first;
    ConeOriginDirection second;

    if (std::abs(bc1OriginZ) > 1e3 * radius) {
        vec3 bc1Origin = vec3(0.f, 0.f, r0.y);
        vec3 bc1Direction = vec3(0.f, 0.f, -1.f);
        mat4 bc1 = buildCylinder(bc1Origin, bc1Direction, r0.x);
        first = {bc1, bc1Origin, bc1Direction};
    } else {
        float angle1 = std::atan(std::abs(b1.x / b1.y));
        vec3 bc1Origin = vec3(0.f, 0.f, bc1OriginZ);
        vec3 bc1Direction = std::abs(angle1 - std::numbers::pi_v<float> / 2) >= 1e-4
                                ? -sign(b1.x) * sign(b1.y) * vec3(0.f, 0.f, 1.f)
                                : vec3(0.f, 0.f, -1.f);
        mat4 bc1 = buildCone(bc1Origin, bc1Direction, angle1);
        first = {bc1, bc1Origin, bc1Direction};
    }

    if (std::abs(bc2OriginZ) > 1e3f * radius) {
        vec3 bc2Origin = vec3(0.f, 0.f, r0.y);
        vec3 bc2Direction = vec3(0.f, 0.f, 1.f);
        mat4 bc2 = buildCylinder(bc2Origin, bc2Direction, r0.x);
        second = {bc2, bc2Origin, bc2Direction};
    } else {
        float angle2 = std::atan(std::abs(b2.x / b2.y));
        vec3 bc2Origin = vec3(0.f, 0.f, bc2OriginZ);
        vec3 bc2Direction = std::abs(angle2 - std::numbers::pi_v<float> / 2) >= 1e-4
                                ? -sign(b2.x) * sign(b2.y) * vec3(0.f, 0.f, 1.f)
                                : vec3(0.f, 0.f, 1.f);
        mat4 bc2 = buildCone(bc2Origin, bc2Direction, angle2);
        second = {bc2, bc2Origin, bc2Direction};
    }
    return {first, second};
}
